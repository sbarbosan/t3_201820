package model.vo;

/**
 * Representation of a Trip object
 */
public class VOTrip {

	
	public int tripId; 
	public int bikeId;
	public float tripDuration;
	public int idFromStation;
	public String fromStation;
	public int idToStation;


	
	
	public VOTrip(int id, int bId, float dur, int idFrom, String from, int idTo)
	{
		tripId = id;
		bikeId = bId;
		tripDuration = dur;
		idFromStation = idFrom;
		fromStation = from;
		idToStation = idTo;
		}
	
	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return tripId;
	}	
	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		// TODO Auto-generated method stub
		return tripDuration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		// TODO Auto-generated method stub
		return fromStation;
	}
	
	/**
	 * @return station_name - Destination Station Name .
	 */

}
