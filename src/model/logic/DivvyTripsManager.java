package model.logic;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.DoublyLinkedList;
import model.data_structures.IDoublyLinkedList;
import model.data_structures.Node;
import model.data_structures.Queue;
import model.data_structures.Stack;

public class DivvyTripsManager<T> implements IDivvyTripsManager {
	
	Queue<VOStation> stations;
	Queue<VOTrip> cola;
	Stack<VOTrip> pila;;
	
	public void loadStations (String stationsFile) {
		stations = new Queue<VOStation>();
		
		try 
		{
			int contador = 1;
			FileReader fileReader = new FileReader(stationsFile);
			BufferedReader reader = new BufferedReader(fileReader);

			reader.readLine();
			String linea = reader.readLine();
			
			while(linea != null)
			{
				
				String[] lineas = linea.split(",");
				int id = Integer.parseInt(lineas[0]);
				String name = lineas[1];
				String city = lineas[2];
				double latitude = Double.parseDouble(lineas[3]);
				double longitude = Double.parseDouble(lineas[4]);
				int dpcapacity = Integer.parseInt(lineas[5]);
				String online_date = lineas[6];
				VOStation station = new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date);
				stations.enqueue(station);
				System.out.println(contador);
				linea = reader.readLine();
				contador ++;
			}
			
			reader.close();
			fileReader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	}
	
	public void loadTrips (String tripsFile) {
		cola = new Queue<VOTrip>();
		pila = new Stack<VOTrip>();
		//Crea la cola
		try 
		{
			int contador = 1;
			FileReader fileReader = new FileReader(tripsFile);
			BufferedReader reader = new BufferedReader(fileReader);

			reader.readLine();
			String linea = reader.readLine();
			
			while(linea != null)
			{
				
				linea = linea.substring(1, linea.length()-1);
				String[] lineas = linea.split('"'+ ","+'"');
				
				VOTrip trip = new VOTrip(Integer.parseInt(lineas[0]), Integer.parseInt(lineas[3]), Float.parseFloat(lineas[4]), Integer.parseInt(lineas[5]), lineas[6], Integer.parseInt(lineas[7]));
				cola.enqueue(trip); 
				pila.push(trip);
				System.out.println(contador);
				linea = reader.readLine();
				contador ++;
			}
			
			reader.close();
			fileReader.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
			
	}
	
	@Override
	public DoublyLinkedList <String> getLastNStations (int bicycleId, int n) {
		
		DoublyLinkedList<String> list = new DoublyLinkedList<String>();
		int contador = 0;
		VOTrip trip = null;
		Iterator<VOTrip> it = cola.iterator();
		while(it.hasNext() && contador < n)
		{
			trip = (VOTrip) it.next();
			System.out.println(trip.bikeId);
			if(trip.bikeId == bicycleId)
			{
				list.addNode(new Node(trip.fromStation));
				contador ++;
			}
		}
		
		return list;
		
	}

	@Override
	public VOTrip customerNumberN (int stationID, int n) {
		
		int contador = 0;
		VOTrip trip = null;
		Iterator<VOTrip> iter = cola.iterator();
		while(iter.hasNext())
		{
			trip = (VOTrip) iter.next();
			if(trip.idFromStation == stationID && contador == n)
			{
				return trip;
			}
		}
		
		return trip;
	}	


}
