package model.data_structures;

import java.util.Iterator;

public class Stack <T> implements Iterable<T>, IStack<T>

{
	public Node<T> first;
	public int size;
	
	
	
	public Stack()
	{
		first = null; 
		size = 0;
	}

	@Override
	public boolean isEmpty() {

		return size == 0;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void push(T t) {
		Node<T> oldFirst = first;
		first = new Node<T>(t);
		first.next = oldFirst;
		size++;
		
	}

	@Override
	public T pop() {
		if(first != null)
		{
			T element = first.element;
			first = first.next;
			size--;
			return element;
		}
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(first);
	}
	
	
	  

}
