package model.data_structures;

import java.util.Iterator;

public class DoublyLinkedList<T> implements IDoublyLinkedList<T> {
	public Node<T> first;
	public Node<T> last;
	
	@Override
	public Iterator iterator() {
		return new ListIterator<T>(first);
		}

	public Node<T> getFirst() {
		return first;
	}
	
	@Override
	public Integer getSize() {
		int contador = 0; 
		if(first == null)
		{
			return contador;			
		}
		else
		{
			for(Node<T> x = first; x != null; x = x.next)
			{
				contador++;
			}
			return contador;
		}
	}

	@Override
	public void addNode(Node<T> n) {
		if(first != null)
		{
			first.previous = n;
			n.next = first;
		}
		else
		{
			last = n;
		}
		first = n;
	}

	@Override
	public void addAtEnd(Node<T> n) {
		if(last != null)
		{
			last.next = n; 
			n.previous = n;
		}
		else 
		{
			first = n;
			last = n;
		}
		
		last = n;
	}

	@Override
	public void AddAtK(Node<T> n, int k) {
		if(first == null)
		{
			first = n;
		}
		else
		{
			Node<T> prev = last;
			int contador = 0;
			for(Node<T> x = first; x != null && contador < k; x = x.next )
			{
				contador ++;
				if(x != null)
				{
					prev = x;
				}
			}
			
			n.next = prev.next;
			prev.next.previous = n;
			prev.next = n;
			n.previous = prev;
		}	
		
	}

	@Override
	public Node getElement() {
		// TODO Auto-generated method stub
		return first;
	}
}
