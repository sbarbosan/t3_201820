package model.data_structures;

import java.util.Iterator;



public  class ListIterator<T> implements Iterator<T> {
    private Node<T> current;

    public ListIterator(Node<T> first) {
        current = first;
    }

    public boolean hasNext() {
        return current != null;
    }

    public void remove() {
        throw new UnsupportedOperationException();
    }

    public T next() {
    	if(hasNext() != false)
    	{
    		T element = current.element;
    		current = current.next; 
    		return element;
    	}
    	return null;
    }
}