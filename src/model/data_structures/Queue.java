package model.data_structures;

import java.util.Iterator;

public class Queue<T> implements Iterable<T>, IQueue<T>
{
	public Node<T> first;
	public Node<T> last;
	public int size;

	
	public Queue()
	{
		first = null;
		last = null;
		size = 0;
	}
	
	@Override
	public boolean isEmpty() {
		
		return first == null;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public void enqueue(T t) {
		Node<T> oldLast = last;
		last = new Node<T>(t);
		last.next = null;
		if(isEmpty()) first = last;
		else oldLast.next = last;
		size++;
		
	}

	@Override
	public T dequeue() {
		if(!isEmpty())
		{
			T element = first.element;
			first = first.next;
			size--;
			if(isEmpty()) last =null;
			return element;
		}
		return null;
	}

	@Override
	public Iterator<T> iterator() {
		return new ListIterator<T>(first);
	}

}
